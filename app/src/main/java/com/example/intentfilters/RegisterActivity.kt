package com.example.intentfilters

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    private val requestCode = 10

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        init()
    }

    private fun init() {
        chooseAddressButton.setOnClickListener {
            openAddressActivity()
        }
        signUpButton.setOnClickListener {
            if (usernameEditText.text.isEmpty() || firstNameEditText.text.isEmpty() || lastNameEditText.text.isEmpty() || ageEditText.text.isEmpty() || addressTextView.text.isEmpty()) {
                Toast.makeText(applicationContext, "Please fill all fields!", Toast.LENGTH_SHORT)
                    .show()
            } else {
                openDashBoard()
            }
        }
    }

    private fun openAddressActivity() {
        val intent = Intent(this, ChooseAddressActivity::class.java)
        startActivityForResult(intent, requestCode)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == this.requestCode && resultCode == Activity.RESULT_OK) {
            val address = data!!.extras!!.getString("address", "")
            addressTextView.text = address
        }
    }

    private fun openDashBoard() {
        val intent = Intent(this, DashBoardActivity::class.java)
        val userModel = UserModel(
            usernameEditText.text.toString(),
            firstNameEditText.text.toString(),
            lastNameEditText.text.toString(),
            ageEditText.text.toString().toInt(),
            addressTextView.text.toString()
        )
        intent.putExtra("userModel", userModel)
        startActivity(intent)


    }
}
