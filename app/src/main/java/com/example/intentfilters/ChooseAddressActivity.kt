package com.example.intentfilters

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import kotlinx.android.synthetic.main.activity_choose_address.*

class ChooseAddressActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_address)
        init()
    }

    private fun init(){
        sendAddressButton.setOnClickListener {
            sendAddress()
        }
    }

    private fun sendAddress(){
        val intent = intent.putExtra("address", addressEditText.text.toString())
        d("address text", intent.toString())
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}
