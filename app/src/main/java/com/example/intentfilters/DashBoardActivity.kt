package com.example.intentfilters

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_dash_board.*

class DashBoardActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dash_board)
        init()
    }

    private fun init() {
        val userModel = intent.extras!!.getParcelable<UserModel>("userModel")
        usernameTextView.text = userModel!!.username
        firstNameTextView.text = userModel.firstName
        lastNameTextView.text = userModel.lastName
        ageTextView.text = userModel.age.toString()
        addressTextView.text = userModel.address

    }
}
